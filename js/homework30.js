

class User {
    status = 'online';
    _age = 23;

    constructor(name) {
        this.name = name;
    }

    sayHi() {
        
        console.log(`Привет, я ${this.name} и мне ${this.#age}`);
    }
}

let timur = new User('Timur');

timur.sayHi();

console.log(timur);
