let fetchPromise = fetch("https://reqres.in/api/users?per_page=12");

let jsonPromise = fetchPromise.then((res) => {
  return res.json();
});

jsonPromise.then((json) => {
  let users = json.data;

  let str = `Получили пользователей: ${users.length} \n`;
  let usersStr = users.map((user) => {
    return `— ${user.first_name} ${user.last_name} (${user.email})`;
  });

  console.log(str + usersStr.join("\n"));
});

jsonPromise.then(({ data: users }) =>
  console.log(
    users.reduce(
      (str, user) =>
        str + `— ${user.first_name} ${user.last_name} (${user.email}) \n`,
      `Получили пользователей: ${users.length} \n`
    )
  )
);
