

let user = {
  name: 'Timur',
  email: 'example@mail.ru'
}

fetch('https://reqres.in/api/users', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(user)
}).then(console.log);

