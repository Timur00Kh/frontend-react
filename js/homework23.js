let i = 0;
while (i < 3) {
  console.log(i);
  // i = i + 1;
  // i += 1;
  i++;
}

function startCalculator() {
  let sum = 0;
  while (true) {
    let value = prompt("Введите значение");

    if (value === null) break;

    if (Number.isNaN(+value)) {
      alert("Введите валидное число!!!");
      continue;
    }

    sum += +value;
  }
  alert("Сумма: " + sum);
}

function factorial(n) {
    let result = 1;
    for (let i = 1; i <= n; i++) {
        result = result * i;
    }
    return result;
}

let userName = "Timur";

if (true) {
    let userName = "Ivan";
    console.log(userName);
}

function sayHi() {
    let userName = "Ivan";

    alert(`Hi, ${userName}`);
}


