const FIELDSET_ERROR_CLASS = "form__fieldset_error";
const INPUT_ERROR_TEXT_CLASS = "form__fieldset-error-text";

class FormController {
  form;

  constructor(form) {
    this.form = form;
    form.addEventListener("submit", this.sendForm.bind(this));
  }

  validateForm() {
    const nameInput = this.form.name;
    const emailInput = this.form.email;

    nameInput.parentElement.classList.remove(FIELDSET_ERROR_CLASS);
    emailInput.parentElement.classList.remove(FIELDSET_ERROR_CLASS);

    let errorCounter = 0;

    if (nameInput.value.length < 3) {
      nameInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
      console.error("имя меньше 3");
      errorCounter++;
      nameInput.parentElement.querySelector(
        "." + INPUT_ERROR_TEXT_CLASS
      ).innerText = "< 3";
    }

    if (nameInput.value.length > 10) {
      nameInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
      console.error("имя > 10");
      errorCounter++;
      nameInput.parentElement.querySelector(
        "." + INPUT_ERROR_TEXT_CLASS
      ).innerText = "> 10";
    }

    if (!emailInput.value.includes("@")) {
      emailInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
      console.error("E-mail не валиден");
      errorCounter++;
    }

    return errorCounter === 0;
  }

  sendForm(event) {
    event.preventDefault();
    this.sendFormWithValidation();
  }

  sendFormWithValidation() {
    const isValid = this.validateForm();
    const nameInput = this.form.name;
    const emailInput = this.form.email;

    if (isValid) {
      console.log({
        email: emailInput.value,
        name: nameInput.value,
      });

      nameInput.value = "";
      emailInput.value = "";
      alert("Форма успешно отправлена!");
    }

    console.log(isValid);
  } 


}

const FORM_LOCAL_STORAGE_KEY = "FORM_LOCAL_STORAGE_KEY";

class FormControllerSaveData extends FormController {
  constructor(form) {
    super(form);
    this.watchInputs();

    const formDataString = localStorage.getItem(FORM_LOCAL_STORAGE_KEY);
    if (formDataString !== null) {
      const nameInput = form.name;
      const emailInput = form.email;
      const formData = JSON.parse(formDataString);

      nameInput.value = formData.name;
      emailInput.value = formData.email;
    }
  }

  watchInputs() {
    const nameInput = form.name;
    const emailInput = form.email;

    [nameInput, emailInput].forEach((input) =>
      input.addEventListener("input", () => {
        const name = nameInput.value;
        const email = emailInput.value;

        localStorage.setItem(
          FORM_LOCAL_STORAGE_KEY,
          JSON.stringify({
            name,
            email,
          })
        );
      })
    );
  }

  sendForm(event) {
    event.preventDefault();
    this.sendFormWithValidation();
    localStorage.removeItem(FORM_LOCAL_STORAGE_KEY);
  }
}

const form = document.getElementById("form");
const FeedbackForm = new FormControllerSaveData(form);

const form2 = document.getElementById("form2");
const FeedbackForm2 = new FormController(form);
