const form = document.getElementById("form");
const FIELDSET_ERROR_CLASS = "form__fieldset_error";
const INPUT_ERROR_TEXT_CLASS = "form__fieldset-error-text";
const nameInput = document.getElementById("name");
const emailInput = document.getElementById("email");
const FORM_LOCAL_STORAGE_KEY = "FORM_LOCAL_STORAGE_KEY";

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const nameInput = event.target.name;
  const emailInput = event.target.email;
  const textInput = event.target.text;
  nameInput.parentElement.classList.remove(FIELDSET_ERROR_CLASS);
  emailInput.parentElement.classList.remove(FIELDSET_ERROR_CLASS);

  let errorCounter = 0;

  if (nameInput.value.length < 3) {
    nameInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
    console.error("имя меньше 3");
    errorCounter++;
    nameInput.parentElement.querySelector(
      "." + INPUT_ERROR_TEXT_CLASS
    ).innerText = "< 3";
  }

  if (nameInput.value.length > 10) {
    nameInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
    console.error("имя > 10");
    errorCounter++;
    nameInput.parentElement.querySelector(
      "." + INPUT_ERROR_TEXT_CLASS
    ).innerText = "> 10";
  }

  if (!emailInput.value.includes("@")) {
    emailInput.parentElement.classList.add(FIELDSET_ERROR_CLASS);
    console.error("E-mail не валиден");
    errorCounter++;
  }

  if (errorCounter === 0) {
    console.log({
      email: emailInput.value,
      name: nameInput.value,
    });

    nameInput.value = "";
    emailInput.value = "";
    localStorage.removeItem(FORM_LOCAL_STORAGE_KEY);
    alert("Форма успешно отправлена!");
  }
});

[nameInput, emailInput].forEach((input) =>
  input.addEventListener("input", () => {
    const name = nameInput.value;
    const email = emailInput.value;

    localStorage.setItem(
      FORM_LOCAL_STORAGE_KEY,
      JSON.stringify({
        name,
        email,
      })
    );
  })
);

const formDataString = localStorage.getItem(FORM_LOCAL_STORAGE_KEY);
if (formDataString !== null) {
  const formData = JSON.parse(formDataString);

  nameInput.value = formData.name;
  emailInput.value = formData.email;
}
