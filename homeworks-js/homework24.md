
## Инструкция по выполнению

- Переключиться в основную ветку проекта `git checkout main`
- Скачать изменения из основной ветки `git pull`
- Создать новую ветку в своём проекте `git branch homework-N`, где `N` - номер домашки
- Переключиться в данную ветку `git checkout homework-N`
- Выполнить основную задачу домашнего задания,отредактировав необходимые файлы
- Добавить изменения в индекс Git `git add .`
- Сделать коммит изменений `git commit -m “Ваше сообщение”`
- Запушить изменения в Gitlab `git push origin homework-N`
- Создать новый merge request в Gitlab (через интерфейс насайте)
- Прикрепить ссылку на merge request и архив в систему обучения для проверки наставником
- После проверки наставника вливаем merge request в основнуюветку проекта (нажимаем кнопку merge)

## 24. Объекты, комментарии и JSDoc

### Задача

Подключить на страницу отдельный файл `homework24.js`, в котором выполнены указанные ниже упражнения по теме данного занятия. Все упражнения выполняются в одном файле, но должны бытьотделены друг от друга с помощью комментария, например:


```js
// Упражнение 1 
let a = 2;
let b = 3;
let result = a + b;
console.log(result);

// Упражнение 2
// ...
```

### Список упражнений 

1. Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false. Должна корректно работать абсолютно для любого объекта. Добавьте для данной функции комментарий в виде JSDoc с описанием того, что она
 делает, какие параметры принимает и что возвращает. Пример правильной работы функции:
 
```js
let user = {};
alert(isEmpty(user)); // true
user.age = 12;
alert(isEmpty(user)); // false
```
2. У нас есть объект, в котором хранятся зарплаты нашей команды:

```js
let salaries = {
 John: 100000,
 Ann: 160000,
 Pete: 130000,
};
```
Необходимо написать функцию `raiseSalary(perzent)`, которая позволит произвести повышение зарплаты на определенный процент и будет возвращать объект с новыми зарплатами. Например, если мы передаем внутрь этой функции число 5, то зарплата каждого сотрудника должна быть повышена на 5% (применить округление до целого числа в меньшую сторону).
После чего необходимо вывести в консоль общий бюджет нашей команды, т.е. суммарное значение всех зарплат после изменения. Добавить для этой функции описание в формате JSDoc.

### Критерии приёмки 

- Скрипт подключён на страницу в виде отдельного JavaScript-файла;
- В скрипте используется строгий режим;
- Внутри скрипта корректно выполнены все перечисленные упражнения.

### Инструкция по выполнению

- [Инструкция по выполнению](#инструкция-по-выполнению)
- Ветка: `homework-24`

